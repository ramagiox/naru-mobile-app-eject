import React, { Component } from 'react';
import { Container, Header, Content, Form, Item, Input, Label, Left,Icon, Button } from 'native-base';
import { AppHeader, AppFooter } from '../app-nav/index';
import {AppRegistry, StyleSheet, Text, TextInput, View} from 'react-native';
export default class Zakat extends Component {
  constructor() {
    super()
    this.state = {gaji:'', bonus:'',pengeluaran:'',
      hasil:'', beras:''}
      
    }
  
  render() {
    let gaji=this.state.gaji;
    let bonus=this.state.bonus;
    let pengeluaran=this.state.pengeluaran;
    let haul=this.state.beras*520;
    let limit = gaji && bonus && pengeluaran ? ((gaji + bonus - pengeluaran)): null;
    let result = limit * 0.025;
    if(haul>limit){
      result="Anda Tidak Wajib Membayar Zakat";
    }
    return (

      <Container style={{backgroundColor:"#FFF"}}>
      
      <AppHeader navigation={this.props.navigation} title="Kalkulator Zakat"/>

        <Content>
        <Text style={styles.titleText}>Data Harta</Text>
          <Form>
            <Item floatingLabel style={{marginLeft:"5%", marginRight:"5%"}}>
              <Label>Penghasilan bersih (Rp)</Label>
              <Input keyboardType='numeric' onChangeText={(text) => this.setState({gaji:parseInt(text)})} />
            </Item>
            <Item floatingLabel style={{marginLeft:"5%", marginRight:"5%"}}>
              <Label>Penghasilan lain (Rp)</Label>
              
              <Input  keyboardType='numeric' onChangeText={(text) => this.setState({bonus:parseInt(text)})} />
            </Item>
            <Item floatingLabel style={{marginLeft:"5%", marginRight:"5%"}}>
              <Label>Pengeluaran (Rp)</Label>
              
              <Input keyboardType='numeric' onChangeText={(text) => this.setState({pengeluaran:parseInt(text)})} />
            </Item>
            <Item floatingLabel style={{ marginLeft:"5%", marginRight:"5%"}}>
              <Label>Harga Beras Saat Ini (per kg)</Label>
              <Input keyboardType='numeric' onChangeText={(text) => this.setState({beras:parseInt(text)})} />
            </Item> 
          </Form>
          <Item rounded style={{marginTop:"10%", marginLeft:"5%", marginRight:"5%"}}>
            <Input disabled placeholder='Harta Zakat'/>{ result ? <Text> { result } </Text> : null }
          </Item>
          
        </Content>
        {/*<AppFooter navigation={this.props.navigation}/>*/}
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  baseText: {
      fontFamily: 'Ubuntu_Light',

  },
  titleText: {
      fontFamily: 'Ubuntu_Regular',
      fontSize: 30,
      fontWeight: 'bold',
      marginTop:"5%",
      marginLeft:"2%"
  },
  container: {
      flex: 0.5,
      justifyContent: 'center',
      alignItems: 'center',
  },
  contentContainer: {
      borderWidth: 0,
      borderColor: '#FFF',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
  footer: {
      backgroundColor: "#fff"
  },
});